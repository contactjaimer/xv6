#include "types.h"
#include "stat.h"
#include "user.h"

int
main(int argc, char **argv)
{
	int fd;

  	//turn on tracing
  	// printf(0, "Trace test on\n");
	trace(1);
	fd = open("file.c", 0);
	getpid();
	close(fd);

	//turn off tracing
	// printf(0, "Turning tracing off with same syscalls as above\n");
	trace(0);
	fd = open("file.c", 0);
	getpid();
	close(fd);

	return 0;
}